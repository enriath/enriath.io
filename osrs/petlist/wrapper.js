petOrder = ["chaos elemental","mole","kbd","zulrah","rex","supreme","prime","kq","zilyana","graardor","kril","kree","sire","kraken","cerb","thermy","chompy","venenatis","scorpia","callisto","vetion","jad","ba","olm","skotizo","corp","phoenix","squirrel","tangleroot","guardian","racoon","golem","heron","beaver","chinchompa","bloodhound","inferno","herbi","noon","vorkath","dust","verzik","hydra","sarachnis","gauntlet","zalcano"]
fancy = ["Chaos Elemental","Giant Mole","King Black Dragon","Zulrah","Dagannoth Rex","Dagannoth Supreme","Dagannoth Prime","Kalphite Queen","Commander Zilyana","General Graardor","K'ril Tsutsaroth","Kree'arra","Abyssal Sire","Kraken","Cerberus","Thermy","Chompy","Venenatis","Scorpia","Callisto","Vet'ion","TzTok Jad","Barbarian Assault","Chambers of Xeric","Skotizo","Corporeal Beast","Wintertodt","Agility","Farming","Runecrafting","Thieving","Mining","Fishing","Woodcutting","Chinchompa","Master Clues","The Inferno","Herbiboar","Dawn/Dusk","Vorki", "Chambers of Xeric: Challenge Mode", "Theatre of Blood", "Alchemical Hydra", "Sarachnis", "Gauntlet", "Zalcano"]
safeNames = new Array();

function generatePetBoxes()
{
	var parent = document.getElementById("background");
	for(var x = 0; x < petOrder.length; x++)
	{
		var safeName = fancy[x].split(" ").join("").split("'").join("")+"KC";
		safeNames.push(safeName);
		var Root = document.createElement("DIV");
		Root.classList.add("petWrapper")

		var labelRoot = document.createElement("DIV")
		labelRoot.classList.add("petLabelRoot")
		Root.appendChild(labelRoot);

		var i = document.createElement("IMG");
		i.src = "pets/"+petOrder[x]+".png"
		i.id = fancy[x];
		labelRoot.appendChild(i);

		var name = document.createElement("SPAN");
		name.innerHTML = fancy[x];
		labelRoot.appendChild(name);

		var inputRoot = document.createElement("DIV")
		inputRoot.classList.add("petInputRoot")
		Root.appendChild(inputRoot);

		var check = document.createElement("INPUT");
		check.type = "checkbox";
		check.name = safeName;
		inputRoot.appendChild(check);

		var entry = document.createElement("INPUT");
		entry.type = "text";
		entry.value = ""
		entry.size = 6
		entry.disabled = true;
		entry.id = safeName;
		inputRoot.appendChild(entry);

		

		check.onchange = function()
		{
			document.getElementById(this.name).disabled = !this.checked;
		}

		parent.appendChild(Root)
	}
}

function generateImage()
{	
	var stage1 = document.getElementById("inputSection");
	stage1.classList.add("hidden");
	var stage1Actions = document.getElementById("input-actions");
	stage1Actions.classList.add("hidden");

	var canvas = document.getElementById("resultCanvas");
	var scale = parseInt(document.getElementById("scaleFactor").value);
	var distance = parseInt(document.getElementById("distance").value);
	var data = new Array();
	for(var i = 0, namelen = safeNames.length; i < namelen; i++)
	{
		var val = document.getElementById(safeNames[i]);
		if(!val.parentNode.children[0].checked)
		{
			data.push("-1");
		}
		else
		{
			data.push(val.value);
		}
	}
	var iX = parseInt(document.getElementById("gridX").value);
	var iY = parseInt(document.getElementById("gridY").value);
	gen(canvas,iX,iY,scale,distance,fancy,data);
	var url = canvas.toDataURL();
	document.getElementById("resultImage").src = url;
	document.getElementById("saveImage").href = url;
	var stage3 = document.getElementById("result");
	stage3.classList.remove("hidden");
	var stage3Actions = document.getElementById("result-actions");
	stage3Actions.classList.remove("hidden");
}

function goBack()
{
	var stage3 = document.getElementById("result");
	stage3.classList.add("hidden");
	var stage3Actions = document.getElementById("result-actions");
	stage3Actions.classList.add("hidden");	

	var stage1 = document.getElementById("inputSection");
	stage1.classList.remove("hidden");
	var stage1Actions = document.getElementById("input-actions");
	stage1Actions.classList.remove("hidden");
}

function readFile(e)
{
	var file = e.target.result, results;
	if(file && file.length)
	{

		results = file.split("\n");
		if(results[0].includes("\t"))
		{
			var children = document.getElementById("background").children;
			for(var l = 0; l < results.length; l++)
			{
				var v = results[l].split("\t")[1]
				if(v != -1)
				{
					children[l].children[1].children[0].checked = true;
					children[l].children[1].children[1].disabled = false;
					children[l].children[1].children[1].value = v;
				}
			}
		}
	}
}

function handleUpload()
{
	var fls = document.getElementById("uploadPets");
	if('files' in fls)
	{
		if(fls.files.length != 0)
		{
			var reader = new FileReader();
			reader.readAsText(fls.files[0]);
			reader.addEventListener("load",readFile);
		}
	}
}

function onLoad()
{
	generatePetBoxes();
	document.getElementById("Generate").addEventListener("click",function()
	{
		generateImage();
	});

	document.getElementById("Go Back").addEventListener("click",function()
	{
		goBack();
	});

	document.getElementById("petsHelp").addEventListener("click",function()
	{
		var p = document.getElementById("petsHelpBody")
		if(p.className.includes("hidden"))
		{
			p.classList.remove("hidden");
		}
		else
		{
			p.classList.add("hidden");
		}
	});
}


document.addEventListener("DOMContentLoaded", onLoad);