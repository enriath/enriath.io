---
layout: default
---

<h1 class="cent-head">IxiaBot Cheat Sheet</h1>

<h2>Simple Commands:</h2>

Manage commands with `!com`

<h3>To add a command:</h3>

`!com add !example Here is an example command` 

Simply adds a command that anyone can use

`!com edit !example Here is an example command`

This does the exact same as above, as `add` and `edit` are interchangable

`!com add !subexample %60% Here is an example command that only subscribers can use`

Adding a number surrounded with `%` after the command will limit the command to a certain tier of user. See these tiers below.


Be careful, as there aren't too many checks in place, and it may be possible to overwrite non-simple commands (!uptime, for example)

<h3>To remove a command:</h3>

`!com remove !example`

`!com delete !example`

Both of these commands do the same thing


<h3>Fun tricks with Simple Commands:</h3>

`{}` denotes a replacement. Here are all the valid characters

`c` is used for how many times the command has been called (see `!cuck`)

`s` is used to get the command's invoker

A positive integer (starting from 1) can be used to pass information into the command's output. These can be combined (and should be combined) with an `s`.

For example, if I had the command `!test`, which said `Hello {s1}!`, if I invoked it, it would have the output `Hello Hydrox6!`. However, if I did `!test IxiaBot` instead, the output would be `Hello IxiaBot!`

Adding an `@{s1}` to the start of commands is a very good idea, as if someone asks about something that's in a command, you can add their name after the command


<h2>Killcount commands</h2>

This command keeps a track of the currently selected killcount.

`!kc` will get the currently selected killcount

`!kc Example Boss` will get the killcount of "Example Boss", if it is tracked

`!kcset 80` will set the currently selected killcount to 80

`!kcset Example Boss` will set the currently selected killcount to "Example Boss" without changing its killcount

`!kcset 80 Example Boss` will set the currently selected killcount to "Example Boss", and also set the amount of kills to 80

`!kclist` will list all the currently tracked killcounts

`!kcrem Example Boss` will stop "Example Boss" from being tracked, if it was being tracked at first

`!kc++` will add 1 to the currently selected killcount

There's also an !uptime command


<h2>User levels:</h2>

* STAFF = 10
* ADMIN = 20
* GLOBAL_MODERATOR = 30
* STREAMER = 40
* MODERATOR = 50
* SUBSCRIBER = 60
* NORMAL = 70
* RESTRICTED = 80
* UNKNOWN = 1000


Please note that the only tiers that are currently tracked are:
STREAMER, MODERATOR, SUBSCRIBER, NORMAL